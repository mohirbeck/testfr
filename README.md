# Запуск

1. Клонирование:

```
git clone https://gitlab.com/mohirbeck/testfr.git

cd testfr
```

2. Необходимо создать .env. Можно скопировать из .env.example

3. Запуск

```
docker-compose up
```

## Документация
http://localhost:8000/docs/

## Дополнительные задачи сделаны

3, 5, 9, 11
