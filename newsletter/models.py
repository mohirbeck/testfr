from django.db import models
from django.core.validators import RegexValidator
from timezone_field import TimeZoneField

class Message(models.Model):
    status_choices = (
        ('pending', 'В очереди'),
        ('sent', 'Отправлено'),
        ('failed', 'Не отправлено'),
    )
    client = models.ForeignKey('Client', on_delete=models.CASCADE, verbose_name="Клиент")
    newsletter = models.ForeignKey('Newsletter', on_delete=models.CASCADE, verbose_name="Рассылка", related_name='messages')
    date_sent = models.DateTimeField(auto_now=True, verbose_name="Дата и время отправки сообщения")
    status = models.CharField(max_length=255, verbose_name="Статус", choices=status_choices, default='pending')
    error = models.CharField(max_length=255, verbose_name="Ошибка отправки", blank=True, null=True)

    def __str__(self):
        return self.client.phone_number
    
    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"
        ordering = ['-date_sent']

class Newsletter(models.Model):
    date_started = models.DateTimeField(verbose_name="Дата и время запуска рассылки")
    date_finished = models.DateTimeField(verbose_name="Дата и время окончания рассылки")
    text = models.TextField(verbose_name="Текст рассылки")
    tag_filter = models.CharField(max_length=255, verbose_name="Фильтр по тегу", blank=True, null=True)
    operator_filter = models.PositiveIntegerField(verbose_name="Фильтр по коду мобильного оператора", blank=True, null=True)
    timezone_sensitive = models.BooleanField(verbose_name="Учитывать часовой пояс клиента", default=False)

    def get_clients(self):
        if self.tag_filter:
            return Client.objects.filter(tag=self.tag_filter)
        elif self.operator_filter:
            return Client.objects.filter(operator_code=self.operator_filter)
        else:
            return Client.objects.all()
    
    def create_messages(self):
        clients = self.get_clients()
        messages = []
        for client in clients:
            try:
                message = Message.objects.get(newsletter=self, client=client)
                if message.status == 'failed':
                    messages.append(message)
            except Message.DoesNotExist:
                messages.append(Message.objects.create(newsletter=self, client=client))
        return messages

    def __str__(self):
        return self.text
    
    class Meta:
        verbose_name = "Рассылка"
        verbose_name_plural = "Рассылки"
        ordering = ['-date_started']

class Client(models.Model):
    phone_number = models.CharField(max_length=11, verbose_name="Номер телефона", unique=True, db_index=True, validators=[RegexValidator(regex='^7[0-9]{10}$', message='Номер телефона должен быть в формате 7XXXXXXXXXX')])
    operator_code = models.PositiveIntegerField(verbose_name="Код мобильного оператора")
    tag = models.CharField(max_length=255, verbose_name="Тег", blank=True, null=True)
    timezone = TimeZoneField(verbose_name="Часовой пояс", choices_display="WITH_GMT_OFFSET", default='Europe/Moscow')

    def __str__(self):
        return self.phone_number
    
    class Meta:
        verbose_name = "Клиент"
        verbose_name_plural = "Клиенты"
