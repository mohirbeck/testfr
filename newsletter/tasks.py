import requests
from datetime import datetime, timedelta
from django.conf import settings
from config.celery import app
from .models import Newsletter, Message

@app.task
def send_message(msg_id: int, text: str, date_finished: datetime):
    message = Message.objects.get(id=msg_id)
    client_dt = datetime.now().replace(tzinfo=message.client.timezone)
    if datetime.strptime(date_finished, "%Y-%m-%dT%H:%M:%SZ") > client_dt and message.status != "sent":
        try:
            response = requests.post(
                f"https://probe.fbrq.cloud/v1/send/{message.id}/",
                headers={
                    "Authorization": f"Bearer {settings.API_TOKEN}",
                },
                json={
                    "id": message.client.id,
                    "phone": message.client.phone_number,
                    "text": text,
                },
            )

            if response.status_code == 200:
                message.status = "sent"
            else:
                message.status = "failed"
                message.error = response.text
        except Exception as e:
            message.status = "failed"
            message.error = str(e)
    else:
        message.status = "failed"
        message.error = "Ran out of time"

    message.save()


@app.task
def send_newsletter(newsletter_id: int):
    newsletter = Newsletter.objects.get(
        id=newsletter_id,
    )

    messages = newsletter.create_messages()

    for message in messages:
        send_message.delay(
            message.id,
            message.newsletter.text,
            message.newsletter.date_finished,
        )


@app.task
def update_newsletter():
    newsletters = Newsletter.objects.filter(
        date_started__lte=datetime.now(),
        date_finished__gte=datetime.now() + timedelta(days=1),
    )

    for newletter in newsletters:
        send_newsletter.delay(
            newletter.id,
        )
