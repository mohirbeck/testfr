from rest_framework.serializers import ModelSerializer
from .models import Newsletter, Client
from timezone_field.rest_framework import TimeZoneSerializerField

class NewsletterSerializer(ModelSerializer):
    class Meta:
        model = Newsletter
        fields = '__all__'

class ClientSerializer(ModelSerializer):
    timezone = TimeZoneSerializerField(use_pytz=False)
    class Meta:
        model = Client
        fields = '__all__'
