from django.urls import path
from . import views

urlpatterns = [
    path('newsletters/', views.NewsletterListCreateAPIView.as_view(), name='newsletter-list'),
    path('newsletters/stats/', views.NewsletterListStatsAPIView.as_view(), name='newsletter-stats-list'),
    path('newsletters/<int:pk>/', views.NewsletterRetrieveUpdateDestroyAPIView.as_view(), name='newsletter-detail'),
    path('newsletters/<int:pk>/stats/', views.NewsletterRetrieveStatsAPIView.as_view(), name='newsletter-stats-detail'),
    path('clients/', views.ClientListCreateAPIView.as_view(), name='client-list'),
    path('clients/<int:pk>/', views.ClientRetrieveUpdateDestroyAPIView.as_view(), name='client-detail'),
]
