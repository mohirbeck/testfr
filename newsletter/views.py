from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.views import APIView
from .models import Newsletter, Client
from .serializers import NewsletterSerializer, ClientSerializer
from rest_framework.response import Response
from drf_yasg.utils import swagger_auto_schema


class NewsletterListCreateAPIView(ListCreateAPIView):
    queryset = Newsletter.objects.all()
    serializer_class = NewsletterSerializer


class NewsletterListStatsAPIView(APIView):
    @swagger_auto_schema(
        responses={
            200: "OK",
            404: "Not found",
        },
        operation_id="Newsletter stats",
        operation_description="Get stats for all newsletters",
    )
    def get(self, request):
            newsletters = Newsletter.objects.all().prefetch_related("messages")
            messages_count = 0
            for newsletter in newsletters:
                messages_count += (
                    newsletter.messages.all().filter(status="sent").count()
                )

            return Response(
                {
                    "messages_count": messages_count,
                    "newsletters_count": newsletters.count(),
                }
            )

class NewsletterRetrieveStatsAPIView(APIView):
    @swagger_auto_schema(
        responses={
            200: "OK",
            404: "Not found",
        },
        operation_id="Newsletter stats detail",
        operation_description="Get stats for a specific newsletter",
    )
    def get(self, request, pk):
            newsletter = Newsletter.objects.get(pk=pk)
            messages_count = (
                newsletter.messages.all().filter(status="sent").count()
            )

            return Response(
                {
                    "messages_count": messages_count,
                    "newsletters_count": 1,
                }
            )


class NewsletterRetrieveUpdateDestroyAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Newsletter.objects.all()
    serializer_class = NewsletterSerializer


class ClientListCreateAPIView(ListCreateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class ClientRetrieveUpdateDestroyAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
