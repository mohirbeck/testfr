import os
from celery import Celery
 
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings')
 
app = Celery('config')
app.config_from_object('django.conf:settings')
 
# Load task modules from all registered Django app configs.
app.autodiscover_tasks()

app.conf.beat_schedule = {
    'Update newsletter': {
        'task': 'newsletter.tasks.update_newsletter',
        'schedule': 5.0,
    },
}
