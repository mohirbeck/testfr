from django.contrib import admin
from django.urls import path, include
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

schema_view = get_schema_view(
   openapi.Info(
      title="Newsletter API",
      default_version='v1',
      description="Newsletter API description",
      contact=openapi.Contact(email="mustaqimovmohir@gmail.com"),
   ),
   public=True,
   permission_classes=[permissions.AllowAny],
   validators=['flex', 'ssv'],
)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(('newsletter.urls', 'newsletter'), namespace='newsletter')),
    path('docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
]
urlpatterns += staticfiles_urlpatterns()
